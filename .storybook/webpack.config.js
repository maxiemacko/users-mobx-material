const path = require('path');

module.exports = ({ config }) => {
  config.resolve.modules.push(path.resolve(__dirname, "../src"));
  config.module.rules = config.module.rules.filter(
    f => f.test.toString() !== '/\\.css$/'
  );
  config.module.rules.push(
    {
      test: /\.css$/,
      loaders: [
        require.resolve('style-loader'),
        {
          loader: require.resolve('css-loader'),
          options: {
            sourceMap: true,
            modules: {
              localIdentName: "[name]__[local]___[hash:base64:5]"
            },
          }
        }
      ],
    },
  );
  return config;
};
