import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';

import RootComponent from './components/RootComponent/RootComponent';
import userStore from "./store/userStore";

const App = () => (
  <Provider userStore={userStore}>
    <RootComponent />
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));
