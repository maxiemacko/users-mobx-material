import { observable, action } from "mobx";

class UserStore {
  @observable userList;
  @observable testString;

  constructor() {
    this.userList = [];
    this.lastUserID = 0;
  }

  @action
  removeUser(userId) {
    this.userList = this.userList.filter(item => item.id !== userId);
  }

  @action
  addUser(user) {
    this.userList.push({ id: this.lastUserID, ...user });
    this.lastUserID++;
  }
}

export default new UserStore();
