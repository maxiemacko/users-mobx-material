import React from 'react';
import { storiesOf } from '@storybook/react';
import UserList from '../components/UserList/UserList';
import testUsers from './testUsers';
import AddUserModal from "../components/AddUserModal/AddUserModal";

storiesOf('UserList', module)
  .add('Test User List', () => <UserList userList={testUsers}/>);

storiesOf('AddUserModal', module)
  .add('Add User Modal', () => <AddUserModal />);
