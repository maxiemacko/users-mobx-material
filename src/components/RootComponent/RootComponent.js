import React, { useState } from 'react';
import { inject, observer } from 'mobx-react';
import { Grid, Button, Input, InputAdornment } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear'

import AddUserModal from '../AddUserModal/AddUserModal';
import UserList from '../UserList/UserList';
import { findEntriesByInputValue } from "../../helpers/utils";

import styles from './RootComponent.css';

const RootComponent = (props) => {
  const [modalOpen, toggleModalOpen] = useState(false);
  const [filterInputValue, setFilterInputValue] = useState('');

  const switchModal = () => toggleModalOpen(!modalOpen);

  return (
    <Grid>
      <Grid
        container
        justify="space-between"
        alignItems="flex-end"
        className={styles.topPanel}
      >
        <div>
          <h2>Filter:</h2>
          <Input
            onChange={e => {setFilterInputValue(e.target.value)}}
            value={filterInputValue}
            endAdornment={
              <InputAdornment
                position="end"
                className={styles.adornment}
              >
                <ClearIcon onClick={() => {setFilterInputValue('')}}/>
              </InputAdornment>
            }
          />
        </div>
        <Button
          variant="contained"
          size="medium"
          color="primary"
          onClick={switchModal}
        >
          Add user
        </Button>
      </Grid>
      <UserList
        deleteUserHandler={(userId) => props.userStore.removeUser(userId)}
        userList={findEntriesByInputValue(props.userStore.userList, filterInputValue)}
      />
      <AddUserModal
        handleAddButtonClick={
          (user) => {
            console.log("click handled");
            props.userStore.addUser(user);
          }
        }
        onClose={switchModal}
        open={modalOpen}
      />
    </Grid>
  );
};

export default inject('userStore')(observer(RootComponent));
