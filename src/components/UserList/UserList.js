import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DeleteIcon from '@material-ui/icons/DeleteForever';

import styles from './UserList.css';

const UserList = ({ userList, deleteUserHandler }) => (
  <Paper>
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>User Id</TableCell>
          <TableCell align="left">Name</TableCell>
          <TableCell align="left">Company</TableCell>
          <TableCell align="left">Phone</TableCell>
          <TableCell />
        </TableRow>
      </TableHead>
      <TableBody>
        {userList.map(userRow => (
          <TableRow key={userRow.id}>
            <TableCell component="th" scope="row">
              {userRow.id}
            </TableCell>
            <TableCell align="left">{userRow.name}</TableCell>
            <TableCell align="left">{userRow.company}</TableCell>
            <TableCell align="left">{userRow.phone}</TableCell>
            <TableCell align="left">
              <DeleteIcon
                className={styles.deleteIcon}
                onClick={() => deleteUserHandler(userRow.id)}
              />
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </Paper>
);

export default UserList;
