import React from 'react';
import { Grid, TextField, Button, Dialog } from '@material-ui/core';
import {
  Formik,
} from 'formik';
import * as Yup from 'yup';

import styles from './AddUserModal.css';

const AddUserModal = ({ open, onClose, handleAddButtonClick, ...props }) =>
  (
    <Dialog
      open={open}
      onClose={onClose}
      {...props}
    >
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className={styles.root}
      >
        <h2 className={styles.modalHeader}>Add your user</h2>
        <Formik
          onSubmit={(values) => {
            handleAddButtonClick(values);
            onClose();
          }}
          initialValues={{
            name: '',
            company: '',
            phone: '',
          }}
          validationSchema={Yup.object().shape({
            name: Yup.string()
              .required('Required'),
            company: Yup.string()
              .required('Required'),
            phone: Yup.number()
              .required('Required'),
          })}
        >
          {
            (props) => {
              const {
                values,
                touched,
                errors,
                handleChange,
                handleBlur,
                handleSubmit,
                dirty,
              } = props;

              return (
                <form onSubmit={handleSubmit}>
                  <div className={styles.modalItem}>
                    <TextField
                      error={touched.name && !!errors.name}
                      id="name"
                      label="Name"
                      value={values.name}
                      onBlur={handleBlur}
                      helperText={errors.name}
                      onChange={handleChange('name')}
                    />
                  </div>
                  <div className={styles.modalItem}>
                    <TextField
                      error={touched.company && !!errors.company}
                      id="company"
                      label="Company"
                      onBlur={handleBlur}
                      helperText={errors.company}
                      value={values.company}
                      onChange={handleChange('company')}
                    />
                  </div>
                  <div className={styles.modalItem}>
                    <TextField
                      error={touched.phone && !!errors.phone}
                      id="phone"
                      label="Phone"
                      type="number"
                      value={values.phone}
                      helperText={errors.phone}
                      onChange={handleChange('phone')}
                    />
                  </div>
                  <div className={styles.modalItem}>
                    <Button
                      variant="contained"
                      size="medium"
                      type="submit"
                      color="primary"
                      disabled={!dirty}
                    >
                      Add user
                    </Button>
                  </div>
                </form>
              )
            }
          }
        </Formik>
      </Grid>
    </Dialog>
  );

export default AddUserModal;
