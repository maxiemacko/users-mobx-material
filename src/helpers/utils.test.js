import { findEntriesByInputValue } from "./utils";

const userList = [
  {
    "id": 0,
    "name": "Mike",
    "company": "IBM",
    "phone": "333-444"
  },
  {
    "id": 1,
    "name": "Edward",
    "company": "Google",
    "phone": "245-754"
  },
  {
    "id": 2,
    "name": "John",
    "company": "Sega",
    "phone": "352-930"
  },
  {
    "id": 3,
    "name": "Edwin",
    "company": "YouTube",
    "phone": "125-891"
  }
];

function preparedTestFunction(inputValue) {
  return findEntriesByInputValue(userList, inputValue);
}

describe('Checking Find entries function', () => {
  test('returns an array without input value', () => {
    expect(preparedTestFunction()).toBeInstanceOf(Array);
  });
  test('returns all values from array without input value', () => {
    expect(preparedTestFunction().length).toBe(userList.length);
  });
  test("returns ONE value for 'win' input value", () => {
    expect(preparedTestFunction('win').length).toBe(1);
  });
  test("returns TWO values for 'ed' input value", () => {
    expect(preparedTestFunction('ed').length).toBe(2);
  });
  test('register independent', () => {
    expect(preparedTestFunction("wIn").length).toBe(1)
  });
})

