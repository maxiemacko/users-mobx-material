export function findEntriesByInputValue(entries = [], inputValue) {
  if (!inputValue){
    return entries;
  }

  return entries.reduce((acc, entryItem) => {
    const entryItemValues = Object.values(entryItem);
    const entryItemValuesIncludeInputValue = entryItemValues.find(item => `${item}`.toLowerCase().includes(inputValue.toLowerCase()));
    if (entryItemValuesIncludeInputValue) {
      return [...acc, entryItem];
    }
    return acc;
  }, []);
}


